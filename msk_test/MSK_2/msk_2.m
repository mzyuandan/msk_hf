clear all;clc;
N=20;%比特数
T=1;%比特周期
fc=10;%载波频率
Fs=100;%采样频率
bitstream=randi([0,1],1,N);%随机生成比特流
bitstream1=2*bitstream-1;%产生双极性比特流 -1,1
%实现差分编码
b0=1;
for i=1:N
    encode_output(i)=b0*bitstream1(i);%对应bk
    b0=encode_output(i);
end
I=[];Q=[];%奇数进I路、偶数进Q路
for i=1:N
    if mod(i,2)~=0
        I=[I,encode_output(i)];
    else
        Q=[Q,encode_output(i)];
    end
end
%用绘图来比较I路和Q路比特流
bit_data=[];
for i=1:N
    bit_data=[bit_data,encode_output(i)*ones(1,T*Fs)];%脉冲成型，认为输入的比特流是矩形的
end
I_data=[];Q_data=[];base_wave=-T:1/Fs:T-1/Fs;
for i=1:N/2
    I_data=[I_data,I(i)*cos(pi*base_wave/(2*T))];%I路:(-T,T)之间的半圆乘I(i)是一个比特周期里面的脉冲成型
    Q_data=[Q_data,Q(i)*cos(pi*base_wave/(2*T))];%Q路与I路一样的脉冲成型波形
end
%Q路延时
number_delay=length(base_wave)/2;%延时一个比特周期，一个比特周期所含有的采样点个数除以2，得到一个比特周期里面点的个数
Q_data1=[zeros(1,number_delay),Q_data(1:length(Q_data)-number_delay)];%Q路延时一个比特周期,与后面信息进行补齐
%得到差分编码输出的比特流,绘图与I路和Q路的信息进行比较
figure();
t=0:1/Fs:N*T-1/Fs;
subplot(3,1,1)
plot(t,bit_data);legend('比特流')
subplot(3,1,2)
plot(t,I_data);legend('I路比特流')
subplot(3,1,3)
plot(t,Q_data1);legend('Q路比特流')
%载波信号
bit_t=0:1/Fs:N*T-1/Fs;%定义时间轴
%定义I路和Q路的载波信号
I_carrier=cos(2*pi*fc*bit_t);
Q_carrier=cos(2*pi*fc*bit_t + pi/2);
I_dot=I_data.*I_carrier;
Q_dot=Q_data1.*Q_carrier;
MSK_signal=I_data.*I_carrier+Q_data1.*Q_carrier;%MSK信号是I路信号与I路载波信号进行点乘加上Q路信号与Q路载波信号进行点乘
figure();
t=0:1/Fs:N*T-1/Fs;
subplot(3,1,1)
plot(t,MSK_signal);legend('MSK信号')
subplot(3,1,2)
plot(t,I_dot);legend('I路信号与I路载波信号进行点乘')
subplot(3,1,3)
plot(t,Q_dot);legend('Q路信号与Q路载波信号进行点乘')
snr=1;%性躁比
%MSK接收信号
MSK_receive=awgn(MSK_signal,snr);%调用awgn函数，加上噪声，得到接收的MSK信号
I_output=MSK_receive.*I_carrier;%进I路时MSK接收信号与I路载波进行点乘
%设计MSK_filter低通滤波器步骤:在commond命令窗口里面输入filterDesigner函数回车进去后，可以看见设计低通滤波器的界面，根据Fs、带通信号比特周期T=1，Fpss取比特周期的两倍即可，Fstop自己给定就可以了，然后点击下方的Design Filter（设计滤波器），然后点击上方的File-Generate MATLAB Code-Filter Design Function（产生一个函数功能，我定义了MSK_filter.m函数）
Hd=MSK_filter;%Hda调用MSK_filter，得到前面的滤波系数a、b
I_filter_output=filter(Hd,I_output);%调用matlab滤波函数filter把I路信号通过滤波出来得到I路滤波信号
Q_output=MSK_receive.*Q_carrier;%Q路与I路同理,二者一样
Q_filter_output=filter(Hd,Q_output);
%得到Q路与I路的滤波信号进行抽样判决
for i=1:N/2
    if I_filter_output((2*i-1)*number_delay)>0 %I路在奇数倍周期进行采样，大于0为1、小于0为-1
        I_recover(i)=1;
    else
        I_recover(i)=-1;
    end
    if Q_filter_output(2*i*number_delay)>0 %Q路在偶数倍周期进行采样，大于0为1、小于0为-1
        Q_recover(i)=1;
    else
        Q_recover(i)=-1;
    end
end  %恢复出I路和Q路的信息
%并/串变换得到最后输出的比特流
bit_recover=[];
for i=1:N
    if mod(i,2)~=0
        bit_recover=[bit_recover,I_recover((i-1)/2+1)];
    else
        bit_recover=[bit_recover,Q_recover(i/2)];
    end
end
%比特流在接收端进行差分译码
for i=1:N
    if i==1
        bit_recover1(i)=bit_recover(i);
    else
        bit_recover1(i)=bit_recover(i)*bit_recover(i-1);
    end
end
%根据差分译码出来的数据和前面输入的比特数据进行比较，绘图看错了几个
recover_data=[];
for i=1:N
    recover_data=[recover_data,bit_recover1(i)*ones(1,T*Fs)];
end
figure();
t=0:1/Fs:N*T-1/Fs;
bit_stream=[];
for i=1:N
    bit_stream=[bit_stream,bitstream1(i)*ones(1,T*Fs)];
end
subplot(2,1,2)
plot(t,bit_stream);legend('原始比特流')
subplot(2,1,1)
plot(t,recover_data);legend('恢复比特流')
