//#include "sparse.h"
//#include "decoder.h"
#include "main.h"
//这里的编码与2304码长的程序稍有不同，在求v的子函数里，注释掉的对应于2304

void group(char *group_in, char group_out[][Z_FAC], int group_size, int group_num)
{
	int i,j;
	for (i=0; i<group_num; i++)
		for (j=0; j<group_size; j++)
			group_out[i][j] = group_in[i*group_size+j];
}

void cal_w(int matrix[][NB_FAC], char u[][Z_FAC], int mb, int kb, int z, char w[][Z_FAC])
{
	int i, j, l;
	int shift;
//	char w[MB_FAC][Z_FAC];

	for(i=0; i<mb; i++)
	{
		for(l=0; l<z; l++)
				w[i][l] = 0;
		for(j=0; j<kb; j++)
		{
			if(matrix[i][j] >= 0)
			{
				shift = matrix[i][j];
				for(l=0; l<z; l++)
					w[i][l] = w[i][l] ^ u[j][(shift+l)%z];
			}
		}
	}
}

void cal_v(int matrix[][NB_FAC], char w[][Z_FAC], int mb, int kb, int z, char v[][Z_FAC])
{

	int i, l;
	int shift;
	int val;
	char v_0[Z_FAC];

	for(l=0; l<z; l++)
		v_0[l] = 0;
	for(l=0; l<z; l++)
		for(i=0; i<mb; i++)
			v_0[l] = v_0[l] ^ w[i][l];

	for (i=1; i<mb-1; i++)
	{
		if( matrix[i][kb] >= 0)   //去找61
		shift = matrix[i][kb];
	}
	for(l=0; l<z; l++)
        v[0][l] = v_0[(z-shift+l)%z];

	
	shift = matrix[0][kb];
	for(l=0; l<z; l++)
		v[1][l] = 0;
	for(l=0; l<z; l++)
		v[1][l] =  w[0][l] ^ v[0][(shift+l)%z];

	for(i=2; i<mb; i++)
	{
		val = matrix[i-1][kb] >= 0;
		shift = matrix[i-1][kb];
		for(l=0; l<z; l++)
			v[i][l] = 0;
		for(l=0; l<z; l++)
			v[i][l] = v[i-1][l] ^ w[i-1][l] ^ (val*v[0][(shift+l)%z]);
	}
}


void encode(int marix[][NB_FAC], char *u, char *c, int mb, int kb, int z)
{
	int i;
	char w[MB_FAC][Z_FAC], v[MB_FAC][Z_FAC], u_group[KB_FAC][Z_FAC];

	group(u, u_group, z, kb);
	cal_w(marix, u_group, mb, kb, z, w);
	cal_v(marix, w, mb, kb, z, v);
	for(i=0; i<InfoLength; i++)
		c[i] = u_group[i/z][i%z];
	for(i=0; i<ParityCheckNum; i++)
		c[i+InfoLength] = v[i/z][i%z];
}
