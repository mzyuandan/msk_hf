#include "float.h"
#include  "main.h"

void bpsk(char *data, char *mod_data)
{
	int i;

	for (i=0; i<REP_LEN; i++)
//	for (i=0; i<InfoLength; i++)
		mod_data[i] = 1 - 2*data[i];
}

void de_bpsk(double *data, double *llratio, double n0)
{
	int i;
	for (i=0; i<REP_LEN; i++)
//	for (i=0; i<InfoLength; i++)
	{
		llratio[i] = 0.707*(4./(ModOrder))*data[i]/n0;
		//llratio[i] = (4./(ModOrder))*data[i]/n0;
		if( fabs(llratio[i]) > TOOLARGE ) llratio[i] = sgn(llratio[i]) * TOOLARGE;
		else if( fabs(llratio[i]) < TOOSMALL) llratio[i] = sgn(llratio[i]) * TOOSMALL;
	}
}

void qpsk(char *data, double *mod_data)
{
	int i, j;
	char data_group[SybLength];
	for (i = 0; i <= SybLength; i++)
	{
		data_group[i] = 0;
		for (j = 0; j<2; j++)
			data_group[i] = data_group[i] | (data[i * 2 + j] << (1 - j));
	}

	for (i = 0; i <= SybLength; i++)
	{
		switch (data_group[i])
		{
		case 0: {mod_data[2 * i] = 1;	mod_data[2 * i + 1] = 0;	break; }
		case 1: {mod_data[2 * i] = 0;	mod_data[2 * i + 1] = 1;	break; }
		case 2: {mod_data[2 * i] = 0;	mod_data[2 * i + 1] = -1;	break; }
		case 3: {mod_data[2 * i] = -1;	mod_data[2 * i + 1] = 0;	break; }
		}
	}
}

void de_qpsk(double *receive, double *LLR, double N0)
{
	int i;
	for (i = 0; i<SybLength; i++)
	{
		double I_new, Q_new;
		I_new = receive[2 * i] * C_qpsk - receive[2 * i + 1] * S_qpsk;
		Q_new = receive[2 * i] * S_qpsk + receive[2 * i + 1] * C_qpsk;

		LLR[i * 2] = (4. / (ModOrder))*Q_new / N0;
		LLR[i * 2 + 1] = (4. / (ModOrder))*I_new / N0;
	}

}


void Eight_psk(char *data, double *mod_data)
{
	int i, j;
	char data_group[SybLength][3];
	for (i = 0; i <= SybLength; i++)
	for (j = 0; j<3; j++)
		data_group[i][j] = data[i * 3 + j];

	for (i = 0; i <= SybLength; i++)
	{
		if ((data_group[i][0] == 0) && (data_group[i][1] == 0) && (data_group[i][2] == 0))
		{
			mod_data[2 * i] = 1;
			mod_data[2 * i + 1] = 0;
		}
		else if ((data_group[i][0] == 0) && (data_group[i][1] == 0) && (data_group[i][2] == 1))
		{
			mod_data[2 * i] = 0.707;
			mod_data[2 * i + 1] = 0.707;
		}
		else if ((data_group[i][0] == 0) && (data_group[i][1] == 1) && (data_group[i][2] == 1))
		{
			mod_data[2 * i] = 0;
			mod_data[2 * i + 1] = 1;
		}
		else if ((data_group[i][0] == 0) && (data_group[i][1] == 1) && (data_group[i][2] == 0))
		{
			mod_data[2 * i] = -1 * 0.707;
			mod_data[2 * i + 1] = 0.707;
		}
		else if ((data_group[i][0] == 1) && (data_group[i][1] == 1) && (data_group[i][2] == 0))
		{
			mod_data[2 * i] = -1;
			mod_data[2 * i + 1] = 0;
		}
		else if ((data_group[i][0] == 1) && (data_group[i][1] == 1) && (data_group[i][2] == 1))
		{
			mod_data[2 * i] = -1 * 0.707;
			mod_data[2 * i + 1] = -1 * 0.707;
		}
		else if ((data_group[i][0] == 1) && (data_group[i][1] == 0) && (data_group[i][2] == 1))
		{
			mod_data[2 * i] = 0;
			mod_data[2 * i + 1] = -1;
		}
		else if ((data_group[i][0] == 1) && (data_group[i][1] == 0) && (data_group[i][2] == 0))
		{
			mod_data[2 * i] = 0.707;
			mod_data[2 * i + 1] = -1 * 0.707;
		}
	}
}

void de_8psk(double *receive, double *LLR, double N0)
{
	int i;
	for (i = 0; i<SybLength; i++)
	{
		double I_new, Q_new;
		I_new = receive[2 * i] * C_8psk - receive[2 * i + 1] * S_8psk;
		Q_new = receive[2 * i] * S_8psk + receive[2 * i + 1] * C_8psk;

		LLR[i * 3] = (4. / (ModOrder))*Q_new / N0;
		LLR[i * 3 + 1] = (4. / (ModOrder))*I_new / N0;
		LLR[i * 3 + 2] = (4. / (ModOrder))*0.707*(fabs(I_new) - fabs(Q_new)) / N0;
	}
}