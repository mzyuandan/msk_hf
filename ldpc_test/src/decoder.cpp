#include "float.h"
#include "sparse.h"
#include "check.h"
#include "decoder.h"
#include  "main.h"
#include <math.h>

//void   iterprp( mod2sparse *H,  double *llratio,  char *dblk,	  double *bprb);

double ExInfo[28][EncodedLength];
char   *gen_file;

/* function:   parallel decoder
   parameter:  *H		Parity check matrix 
               *llratio  Likelihood ratios for bits 
               *dblk    point to the Place store code decoded
               *pchk    point to the Place to store parity checks  
               *bprb    point to the place to store bit probabilities */
 
 
 unsigned prprp_decode
( mod2sparse *H,	 
  double *llratio,	
  char *dblk,		
  char *pchk,		
  double *bprb,
  char *info,
  int *csum
)
{   int N, n,k,c=0,flag,num,j;
    extern int  Un_BER,De_BER,Un_FER,De_FER;
    extern char DecodedData[EncodedLength];
    extern int  Parity[ParityCheckNum][H_ColNum];

   N = mod2sparse_cols(H);
   initprp(H,llratio,dblk,bprb);
   for (n = 0;n<max_iter; n++)
  {  c = check(H,dblk,pchk);
    if (c==0)
       break; 
	iterprp(H,llratio,dblk,bprb);
  } 

	*csum = c;

	flag=0;
   for(k = 0; k <InfoLength; k++ )
   	   if( info[k] != DecodedData[k] )
	   {	if(c==0) {
		   Un_BER++;
		   num=0;
		   for(j=0;j<EncodedLength;j++)
			   if (DecodedData[j]!=info[k]) {
				   num++;
				   //fprintf(file,"%6d ",j);
				   
			   }
                 //fprintf(file,"%d\n",num);
	   
	   }
	        else
			De_BER++;
	        flag=1;
	   }

	if(flag==1)
	{
		if(c==0)
			Un_FER++;
		else
			De_FER++;
	}
  return n;
} 
 
 
/* function:  initialize probability propagation
   parameter: *H Parity check matrix 
              *llratio Likelihood ratios for bits
              *dblk point to the Place to store decoding 
              *bprb Place to store bit probabilities, 0 if not wanted */    
 void initprp
( mod2sparse *H,	
  double *llratio,	
  char   *dblk,		
  double *bprb		
)
{ 
  mod2entry *e;
  int N;
  int j;
  N = mod2sparse_cols(H);
  for (j = 0; j<N; j++)
  {  for (e = mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e = mod2sparse_next_in_col(e))
	  {	  e->pr = llratio[j];
          e->lr = 0;
		  e->pr1 = llratio[j];
          e->lr1 = 0;
	  }
	  if (bprb) bprb[j] = 1/(1+exp(llratio[j]));
      dblk[j] = !(llratio[j]>=0);				//对数似然比定义为log(p0/p1)
	  //dblk[j] = !(lratio[j]>=1);				//似然比定义为p0/p1
   }
}


double Basic_Calculate(double x,double y)
{	double z;
//	z=(x+y)/(1.+x*y);			
	z=(1.+x*y)/(x+y);
	return z;
}

double min_sum(double x, double y)
{
	double z;
	double x_a, y_a;
//	double x_s, y_s, xy_min, z1, z2;
	x_a = fabs(x);
	y_a = fabs(y);
//	x_s = sgn(x);
//	y_s = sgn(y);
//	xy_min = min(x_a, y_a);
//	z1 = x_s * y_s * xy_min;
//	z = sgn(x) * sgn(y) * min(x_a, y_a);
	z = sgn(x) * sgn(y) * min(x_a, y_a);
	return z;
}

double log_cal(double x)
{
	double y;
	if (fabs(x) <= limit)
		y = TOOLARGE;
	else if (fabs(x) == TOOLARGE)
		y = limit;
	else
		y = log((exp(fabs(x)) + 1)/(exp(fabs(x)) - 1));
	return y;
}


 /***********************************
 //  对数似然比测度的和积算法
 // 
 //
 ***********************************/
void iterprp
( mod2sparse *H,
  double *llratio,
  char *dblk,	
  double *bprb
)
{ double pr, dl;  
  int sl; 
  mod2entry *e;
  int N, M;
  int i, j;

  M = mod2sparse_rows(H);
  N = mod2sparse_cols(H);

  for( i = 0;i < M;i++ )
  {   dl = limit;
	  sl = 1;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	  e->lr = sl * log_cal(dl);
          dl = dl + log_cal(e->pr);
		  sl = sl * sgn(e->pr);
	   }
      dl = limit;
	  sl = 1;
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  { 
		  e->lr = sl * sgn(e->lr) * log_cal(dl + log_cal(e->lr));
 	      dl = dl + log_cal(e->pr);
		  sl = sl * sgn(e->pr);
	  }
  }

  for( j = 0; j < N;j++ )
  {	  pr = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { e->pr = pr;
        pr += e->lr;
		if( fabs(e->pr) > TOOLARGE ) e->pr = sgn(e->pr) * TOOLARGE;
	    else if( fabs(e->pr) < TOOSMALL) e->pr = sgn(e->pr) * TOOSMALL;
	  }
     if( bprb ) bprb[j] = 1/(1+exp(pr));					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(pr>=0);
     pr = 0.0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	e->pr += pr;
        pr += e->lr;
		if( fabs(e->pr) > TOOLARGE ) e->pr = sgn(e->pr) * TOOLARGE;
	    else if( fabs(e->pr) < TOOSMALL) e->pr = sgn(e->pr) * TOOSMALL;
    }
  }
 }

 /***********************************
 //  最小和算法
 // 
 //
 ***********************************/
void iterprp_ms
( mod2sparse *H,
  double *llratio,
  char *dblk,	
  double *bprb	
)
{ double pr, dl;   
  mod2entry *e;
  int N, M;
  int i, j;
  M = mod2sparse_rows(H);
  N = mod2sparse_cols(H);
  for( i = 0;i < M;i++ )
  {   dl = TOOLARGE;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	  e->lr = dl;
          dl = min_sum( dl, e->pr );
	   }
      dl = TOOLARGE;
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  { 
		  e->lr = min_sum( dl, e->lr );
 	      dl = min_sum( e->pr, dl );
	  }
  }

  for( j = 0; j < N;j++ )
  {	  pr = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { e->pr = pr;
        pr += e->lr;
		if( fabs(e->pr) > TOOLARGE ) e->pr = sgn(e->pr) * TOOLARGE;
	    else if( fabs(e->pr) < TOOSMALL) e->pr = sgn(e->pr) * TOOSMALL;
	  }
     if( bprb ) bprb[j] = 1/(1+exp(pr));					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(pr>=0);
     pr = 0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	e->pr += pr;
        pr += e->lr;
		if( fabs(e->pr) > TOOLARGE ) e->pr = sgn(e->pr) * TOOLARGE;
	    else if( fabs(e->pr) < TOOSMALL) e->pr = sgn(e->pr) * TOOSMALL;
    }
  }
}


 /***********************************
 //  对数似然比测度的和积算法
 // 
 //  转换到似然比测度域计算
 ***********************************/
void iterprp_lr
( mod2sparse *H,
  double *llratio,
  char *dblk,	
  double *bprb	
)
{ double pr, dl;   
  mod2entry *e;
  int N, M;
  int i, j;
  M = mod2sparse_rows(H);
  N = mod2sparse_cols(H);
  for( i = 0;i < M;i++ )
  {   dl = exp(double(TOOLARGE));
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
	  {	  e->lr = log(dl);
          dl = Basic_Calculate( dl, exp(e->pr) );
		  if( fabs(e->lr) > TOOLARGE ) e->lr = sgn(e->lr) * TOOLARGE;
	    else if( fabs(e->lr) < TOOSMALL) e->lr = sgn(e->lr) * TOOSMALL;
	   }
      dl = exp(double(TOOLARGE));
      for (e = mod2sparse_last_in_row( H, i ); !mod2sparse_at_end( e );e = mod2sparse_prev_in_row(e) )
	  { 
		  e->lr = log(Basic_Calculate( dl, exp(e->lr) ));
	      dl = Basic_Calculate( exp(e->pr), dl );
		  if( fabs(e->lr) > TOOLARGE ) e->lr = sgn(e->lr) * TOOLARGE;
	    else if( fabs(e->lr) < TOOSMALL) e->lr = sgn(e->lr) * TOOSMALL;
	  }
  }

  for( j = 0; j < N;j++ )
  {	  pr = llratio[j];
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
	  { e->pr = pr;
        pr += e->lr;
		if( fabs(e->pr) > TOOLARGE ) e->pr = sgn(e->pr) * TOOLARGE;
	    else if( fabs(e->pr) < TOOSMALL) e->pr = sgn(e->pr) * TOOSMALL;
	  }
     if( bprb ) bprb[j] = 1/(1+exp(pr));					//在此处作出判决，此时pr=llratio * e->lr1 * e->lr2 * ... * e->lrM
     dblk[j] = !(pr>=0);
     pr = 0.0;
     for ( e = mod2sparse_last_in_col(H,j);!mod2sparse_at_end(e);e=mod2sparse_prev_in_col(e))
    {	e->pr += pr;
        pr += e->lr;
		if( fabs(e->pr) > TOOLARGE ) e->pr = sgn(e->pr) * TOOLARGE;
	    else if( fabs(e->pr) < TOOSMALL) e->pr = sgn(e->pr) * TOOSMALL;
    }
  }
}

