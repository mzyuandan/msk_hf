#include "main.h"
#include "channel.h"

/*function: generate the guass noise
 parameter:no the guass noise power*/
double gauss(double n0)
{	 double u1,u0;
	 double gaussnoise;
	 u0 = (double)rand()/(double)RAND_MAX;
     u1 = (double)rand()/(double)RAND_MAX;
	 if( u0 < limit) u0 = limit;
	 gaussnoise = sqrt(n0*log(1./u0) )*cos( 2*pi*u1 );
     return gaussnoise;
}
/*function: simulating the process of the sigal being trasformed in the guass channel
  parameter:*codebit  pointer point to the codewords to be transformed
            *receive  poiter point to the recived sigal
			lratio    the llikelihood ratio of 0 to 1
            llratio    the log likelihood ratio of 0 to 1	
            n0        the noise power */
void awgn(char *transmit, double *receive, double n0)
{  int i;
  for(i = 0;i < REP_LEN; i++ )
	  receive[i] = transmit[i]+gauss(n0);						
}

void ComplexGauss(double n0, complex_t *cg)
{
    double u1,u0;
	 u0 = (double)rand()/(double)RAND_MAX;
     u1 = (double)rand()/(double)RAND_MAX;
	 if( u0 < limit) u0 = limit;
	 cg->real = sqrt( n0*log(1./u0) )*cos( 2*pi*u1 );
	 cg->imag = sqrt( n0*log(1./u0) )*sin( 2*pi*u1 );
	 return;
}

void rayleigh(double n0, complex_t *codeword, complex_t *receive)
{
	int k = 0;
    complex_t cg;
	complex_t temp;
	
	for (k = 0; k < EncodedLength; k++) 
	{  
		ComplexGauss(1, &cg);	
		temp.real = cg.real * codeword[k].real - cg.imag * codeword[k].imag;
		temp.imag = cg.imag * codeword[k].real + cg.real * codeword[k].imag;
		ComplexGauss(n0, &cg);
		receive[k].real = temp.real + cg.real;
		receive[k].imag = temp.imag + cg.imag;
	}
}

void code_ch(char *transmit, double *receive)
{  int i;
  for(i = 0;i < InfoLength; i++ )
	  receive[i] = transmit[i];
  
  for(i = InfoLength;i < EncodedLength; i++ )
	  receive[i] = 0;						
}


