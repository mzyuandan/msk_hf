int check_qc(int matrix[][NB_FAC],	char *dblk, char *pchk, int nb, int z);
int check (mod2sparse *, char *, char *);
double changed (double *, char *, int);
double expected_parity_errors (mod2sparse *, double *);
double loglikelihood (double *, char *, int);
double expected_loglikelihood (double *, double *, int);
double entropy (double *, int);
