#include "main.h"
#include "sparse.h"
/* the details of the functions below can be refered to
 Software for Low Density Parity Check Codes*/
static mod2entry *alloc_entry( mod2sparse *m)
{ mod2block *b;
  mod2entry *e;
  int k;
  if ( m->next_free == 0)
  { b = (mod2block *) calloc(1,sizeof *b);
    b->next = m->blocks;
    m->blocks = b;
    for ( k = 0; k < Mod2sparse_block; k++ )
    { b->entry[k].left = m->next_free;
      m->next_free = &b->entry[k];
    }
  }
  e = m->next_free;
  m->next_free = e->left;
  e->pr = 0;
  e->lr = 0;
  return e;
}

mod2sparse *mod2sparse_allocate (int n_rows, int n_cols)
{	mod2sparse *m;
	mod2entry  *e;
	int i,j;
	if( ( n_rows <= 0 ) || ( n_cols <= 0 ) )
	{	printf("mod2sparse_allocate:invalid number of rows and columns\n");
        exit(1);
	}
	m = (mod2sparse *) calloc(1,sizeof *m);
	m->n_cols = n_cols;
	m->n_rows = n_rows;
	m->cols = (mod2entry *) calloc(n_cols,sizeof (*(m->cols)));
	m->rows = (mod2entry *) calloc(n_rows,sizeof (*(m->rows)));
	m->blocks = 0;
	m->next_free = 0;
	for( i = 0; i < n_rows; i++ )
	{	e = &(m->rows[i]);
		e->left = e->right = e->up = e->down = e;
		e->col = e->row = -1;
	}
	for( j = 0; j < n_cols; j++ )
	{	e = &(m->cols[j]);
		e->left = e->right = e->up = e->down = e;
		e->col = e->row = -1;
	}
	return m;
}

mod2entry *mod2sparse_insert ( mod2sparse *m, int row, int col )
{  mod2entry *re, *ce, *ne;
  if ( row<0 || row>=mod2sparse_rows(m) || col<0 || col>=mod2sparse_cols(m))
  { printf("mod2sparse_insert: row or column index out of bounds\n");
    exit(1);
  }
  re = mod2sparse_last_in_row(m,row);
  if (!mod2sparse_at_end(re) && mod2sparse_col(re)==col)
   return re;
  if (mod2sparse_at_end(re) || mod2sparse_col(re)<col)
   re = re->right;
  else
  {  re = mod2sparse_first_in_row( m, row );
     for (;;)
	 {  if (!mod2sparse_at_end(re) && mod2sparse_col(re)==col) 
          return re;
        if ( mod2sparse_at_end(re) || mod2sparse_col(re)>col )
          break;
        re = mod2sparse_next_in_row(re);
    }
  }
  ne = alloc_entry(m);
  ne->row = row;
  ne->col = ne->TempCol = col;
  ne->left = re->left;
  ne->right = re;
  ne->left->right = ne;
  ne->right->left = ne;
  ce = mod2sparse_last_in_col(m,col);
  if (!mod2sparse_at_end(ce) && mod2sparse_row(ce)==row)		//该列已经结束，但是该行却还未结束，元素无法放置
  { printf("mod2sparse_insert: Garbled matrix\n");
    exit(1);
  }
  if (mod2sparse_at_end(ce) || mod2sparse_row(ce)<row) 
   ce = ce->down;
  else
  { ce = mod2sparse_first_in_col(m,col);
    for (;;)
    {  if (!mod2sparse_at_end(ce) && mod2sparse_row(ce)==row) 
      { printf("mod2sparse_insert: Garbled matrix\n");
        exit(1);
      }
       if (mod2sparse_at_end(ce) || mod2sparse_row(ce)>row){ break;} 
       ce = mod2sparse_next_in_col(ce);
    }
  }
  ne->up = ce->up;
  ne->down = ce;
  ne->up->down = ne;
  ne->down->up = ne;
  return ne;
}

mod2sparse *Make_LDPC(int Matrix[][H_ColNum],int StartRow,int RowNumber,int ColNumber)
{   int i,j;
	int row,col;
	int EndRow=StartRow+RowNumber;
	mod2sparse *H;
	H=mod2sparse_allocate(RowNumber,ColNumber);
	for(i = StartRow; i < EndRow; i++ )
	{	for(j = 0;j < H_ColNum; j++ )
	    {  if( Matrix[i][j] >= 0 )
			{ 	row  =i-StartRow;
			    col = Matrix[i][j];
                mod2sparse_insert( H, row, col );
			}
		}
	}
 	return H;
}

mod2sparse *Make_QCLDPC(int Matrix[][NB_FAC],int nb, int mb, int z)
{   int i,j;
	int row,col;
	int ColNumber = nb * z;
	int RowNumber = mb * z;
	int i_div, i_mod;
	mod2sparse *H;
	H=mod2sparse_allocate(RowNumber,ColNumber);
	for(i = 0; i < RowNumber; i++ )
	{	
		i_div = i / z;
		i_mod = i % z;
		for(j = 0;j < nb; j++ )
	    {  if( Matrix[i_div][j] >= 0 )
			{ 	
				row = i;
			    col = (j * z) + ((Matrix[i_div][j] + i_mod) % z);
                mod2sparse_insert( H, row, col );
				//printf("i=%d, j=%d, shift=%d, row=%d, col=%d\n", i, j, Matrix[i_div][j], row, col);
			}
		}
		//printf("\n");
	}
 	return H;
}

double degree(mod2sparse *H, double *lam, double *rou)
{   
  mod2entry *e;
  int N, M;
  int i, j;
  int dr=0, dl=0, e_row=0, e_col=0;
  double sum_lam=0, sum_rou=0, v_num=0, c_num=0;
  int row_w[ParityCheckNum];
  int col_w[EncodedLength];
  double rate;

  M = mod2sparse_rows(H);
  N = mod2sparse_cols(H);
  for( i = 0;i < M;i++ )
  {   
	  row_w[i] = 0;
      for ( e = mod2sparse_first_in_row(H,i); !mod2sparse_at_end(e); e = mod2sparse_next_in_row(e) )
		  row_w[i]++;
	  if (dr < row_w[i])
		  dr = row_w[i];
	  e_row += row_w[i];
  }

  for( j = 0; j < N;j++ )
  {	  
	  col_w[j] = 0;
      for(e=mod2sparse_first_in_col(H,j);!mod2sparse_at_end(e); e=mod2sparse_next_in_col(e))
		  col_w[j]++;
	  if (dl < col_w[j])
		  dl = col_w[j];
	  e_col += col_w[j];
  }

  if (e_row != e_col)
  {
	  printf("error: row and col not match!\n");
	  rate = 0.0;
  }
  else
  {
	  for (i=0; i<dr; i++)
	  {
		  rou[i] = 0;
		  for (j=0; j<M; j++)
		  {
			  if (row_w[j] == i+1)
				  rou[i] += (i+1);
		  }
		  rou[i] = rou[i] / e_row;
		  sum_rou += rou[i];
	  }
//	  for (i=0; i<dr; i++)
//		  rou[i] = rou[i] / sum_rou;
	  for (i=0; i<dl; i++)
	  {
		  lam[i] = 0;
		  for (j=0; j<N; j++)
		  {
			  if (col_w[j] == i+1)
				  lam[i] += (i+1);
		  }
		  lam[i] = lam[i] / e_col;
		  sum_lam += lam[i];
	  }
//	  for (i=0; i<dl; i++)
//		  lam[i] = lam[i] / sum_lam;
	  if (sum_rou != 1)
		  printf("error: sum_rou is not equal to 1\n");
	  if (sum_lam != 1)
		  printf("error: sum_rou is not equal to 1\n");
	  
	  lam[10] = dl;
	  rou[10] = dr;

	  for (i=0; i<dl; i++)
	  {
		  v_num +=  lam[i] / (i+1);
	  }
	  for (j=0; j<dr; j++)
	  {
		  c_num += rou[j] / (j+1);
	  }
	  rate = (v_num-c_num) / v_num;
  } 
  return rate;
}

/* MULTIPLY VECTOR BY SPARSE MATRIX. */
void mod2sparse_mulvec
( mod2sparse *m,	
  char *u,												//输入校验矩阵m，判决码字u，
  char *v												//输出每个校验方程的结果v。
)
{ mod2entry *e;
  int M, N;
  int i, j;												
  M = mod2sparse_rows( m );								//M是校验方程的个数
  N = mod2sparse_cols( m );								//N是码字长度
  for (i = 0; i<M; i++)										
	  v[i] = 0;											//v(i)是第i个校验方程的结果，0表示满足。
  for (j = 0; j<N; j++)									
  { if (u[j])											//对所有不为1的码字bit，参加包含其的校验方程
    { for (e = mod2sparse_first_in_col(m,j);
           !mod2sparse_at_end(e);
           e = mod2sparse_next_in_col(e))
       v[mod2sparse_row(e)] ^= 1;						//校验方程的结果加1（模2）
     }
  }
}

