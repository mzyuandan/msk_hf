#include  <string.h>
#include  <malloc.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <time.h>
#include  <math.h>

#define Totalblock1      4000
#define Totalblock2      20000  //the number of total block of the code processed 
#define Totalblock3      9000
#define NB_FAC			 16		
#define KB_FAC			 4		
#define MB_FAC			 12		
#define Z_FAC			 48		//24, 45, 48, 90
#define Z_MAX			 128
#define EncodedLength    NB_FAC*Z_FAC	//8192//2048//384//2304 //encoding length3060//
#define InfoLength		 KB_FAC*Z_FAC	//4096//1024//192//1152 //information length1530//
#define ParityCheckNum   MB_FAC*Z_FAC//4096//1024//192//1152// 1530//

#define REP_NUM			 1
#define REP_LEN			 (EncodedLength*REP_NUM)

#define zero_len		 0
#define InfoLength_real  InfoLength-zero_len
#define G_COLMN			 EncodedLength
#define offset			 (EncodedLength - ParityCheckNum)
#define C_ROW			 (offset + 1)

#define ModOrder         1		
#define SybLength	(EncodedLength/ModOrder)

#define H_ColNum         7   //6//column number of the check matrix
#define CodeRate         (1.0/4) //note the code rate should in double else N0=snreal/code rate?
#define Totalerror		 100 //100
#define max_iter         50//50  //100     // Maximum number of iterations of decoding to do 
#define limit            2.777578167429134e-11
#define limit2           1e300
#define pi               3.1415926
#define TOOLARGE         25
//#define TOOLARGE         7.9375//7.9375//15.875
#define TOOLARGE_E		 1e7
#define TOOSMALL         1e-7
#define DES_BLK			 2
#define VMAX_VAL			 7.9375//15.875
#define BIT_W_VAL			 7
#define QMAX				 63		//2^6-1
#define QSTEP				 0.125//0.25
#define BETA				1*0.125
#define PRQ_BETA			0.1

#define ALPHA			1.0

#define sgn(x) ((2 * (x>=0)) - 1)			//the sign of x
#define min(x, y) ((x<=y) ? x : y)			//the min of x and y
#define sgnx(x) (x<=0)
#define max(x, y) ((x>=y) ? x : y)


void Read_Matrix(int T[][NB_FAC]);

#define S_8psk sin(pi/8)
#define C_8psk cos(pi/8)
#define S_qpsk sin(pi/4)
#define C_qpsk cos(pi/4)























