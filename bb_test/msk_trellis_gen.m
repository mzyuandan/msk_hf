function TREL = msk_trellis_gen(g_t, Ts, sps, h_mod, ca_tilt, fig_on) 
%生成MSK调制对应的trellis结构

load 'trel_test.mat';

TREL.numInputSymbols = 2;
TREL.numOutputSymbols = 2;
TREL.numStates = 2;
TREL.OverSampling = 8;
TREL.nextStates = [1, 0; 0, 1];

out = zeros(TREL.numInputSymbols, sps+1, TREL.numStates);
out1 = zeros(TREL.numInputSymbols, sps+1, TREL.numStates);

out(1, :, 1) = bm_cal(0, 1, g_t, Ts, sps, h_mod, ca_tilt);
[tp, ~] = msk_mod([-1 1 -1], g_t, Ts, sps, h_mod);
out1(1, :, 1) = tp(9:17) .* ca_tilt(9:17);

out(2, :, 1) = bm_cal(0, -1, g_t, Ts, sps, h_mod, ca_tilt);
[tp, ~] = msk_mod([-1 -1 -1], g_t, Ts, sps, h_mod);
out1(2, :, 1) = tp(9:17) .* ca_tilt(9:17);

out(1, :, 2)= bm_cal(pi, 1, g_t, Ts, sps, h_mod, ca_tilt);
[tp, ~] = msk_mod([1 1 -1], g_t, Ts, sps, h_mod);
out1(1, :, 2) = tp(9:17) .* ca_tilt(9:17);

out(2, :, 2) = bm_cal(pi, -1, g_t, Ts, sps, h_mod, ca_tilt);
[tp, ~] = msk_mod([1 -1 -1], g_t, Ts, sps, h_mod);
out1(2, :, 2) = tp(9:17) .* ca_tilt(9:17);

if fig_on
    figure; plot(real(out(1, :, 1))); hold on; plot(real(out(1, :, 1)), 'r');
    figure; plot(imag(out(1, :, 1))); hold on; plot(imag(out(1, :, 1)), 'r');
    figure; plot(real(out(2, :, 1))); hold on; plot(real(out(2, :, 1)), 'r');
    figure; plot(imag(out(2, :, 1))); hold on; plot(imag(out(2, :, 1)), 'r');
    figure; plot(real(out(1, :, 2))); hold on; plot(real(out(1, :, 2)), 'r');
    figure; plot(imag(out(1, :, 2))); hold on; plot(imag(out(1, :, 2)), 'r');
    figure; plot(real(out(2, :, 2))); hold on; plot(real(out(2, :, 2)), 'r');
    figure; plot(imag(out(2, :, 2))); hold on; plot(imag(out(2, :, 2)), 'r');
end
TREL.outputs = out;


%%	Pre-compute {contributing prior-states, corresponding i/p bitvec, branch
%%	o/p bitvec} for each current-state. Reqd for forward-recursion. This is
%%	independent of RX.
TREL.prevStates = NaN(TREL.numStates, TREL.numInputSymbols);
TREL.prevStateIn = NaN(TREL.numStates, TREL.numInputSymbols);
TREL.prevStateOut = NaN(TREL.numInputSymbols, sps+1, TREL.numStates);
for dest_st = 0 : (TREL.numStates -1)
    % Find all origin-states from-which there could be a transition into dest_st
    % and store in row-(dest_st +1)
    [row, col] = find(TREL.nextStates == dest_st);
    TREL.prevStates(dest_st +1, :) = row' -1; % origin-state of transition
    TREL.prevStateIn(dest_st +1, :) = col' -1; % Corresponding i/p bit-vec causing transition
    TREL.prevStateOut(1, :, dest_st+1) = TREL.outputs(col(1), :, row(1));
    TREL.prevStateOut(2, :, dest_st+1) = TREL.outputs(col(2), :, row(2));
end
         