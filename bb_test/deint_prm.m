function [ y ] = deint_prm( x, p )
%interleave based on Coprime numbers
N = length(x);
y = zeros(1, N);
for ii=1:1:N
    inter_i = mod(p*(ii-1), N)+1;
    y(ii) = x(inter_i);
end
end

