function [hl_dec, hl_enc]= Get_SH_w2(MB,NB,Z,Z_MAX, file_name)


spa_r = [];
spa_c = [];

%    读取原始的矩阵,并对QC_H进行不同Z_FAC下的模运算
QC_H = dlmread(file_name);
QC_H1 = QC_H(1:MB,:);
QC_H2 = QC_H(MB+1:MB+2,:);
cnt = 0;
for ii = 1 : MB
    for jj = 1 : NB
        if QC_H1(ii,jj)>=0
            tp = floor(QC_H1(ii,jj)*(Z/Z_MAX));
            QC_H1(ii,jj) = tp;
            for kk=1:1:Z
                cnt = cnt + 1;
                spa_r(cnt) = (ii-1)*Z + kk; 
                spa_c(cnt) = (jj-1)*Z + mod(tp+kk-1, Z) + 1;
            end
        end
        if QC_H2(ii,jj)>=0
            tp = floor(QC_H2(ii,jj)*(Z/Z_MAX));
            QC_H2(ii,jj) = tp;
            for kk=1:1:Z
                cnt = cnt + 1;
                spa_r(cnt) = (ii-1)*Z + kk; 
                spa_c(cnt) = (jj-1)*Z + mod(tp+kk-1, Z) + 1;
            end
        end
    end
end

spa = sparse(spa_r, spa_c, 1);
% hl_enc = fec.ldpcenc(spa);
% hl_dec = fec.ldpcdec(spa);
% 
% hl_dec.decisionType = 'Soft decision';
% hl_dec.outputFormat = 'Whole codeword';
% hl_dec.doParityChecks = 'Yes';

% hl_enc = comm.LDPCEncoder(spa);
hl_enc = 0;
hl_dec = comm.LDPCDecoder(spa);

hl_dec.OutputValue = 'Whole codeword';
hl_dec.DecisionMethod = 'Soft decision';
hl_dec.IterationTerminationCondition = 'Parity check satisfied';
hl_dec.NumIterationsOutputPort = true;
hl_dec.FinalParityChecksOutputPort = true;



