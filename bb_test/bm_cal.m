function [trans_state] = bm_cal(state,alpha,g_t,Ts,os, modulation_index,ca_tilt)

Nbits          = length(alpha);
bits_s         = upsample(alpha,os);
t_seq          = 0:Ts*3000:Nbits;
S_N            = conv(bits_s,g_t); 
S_N            = S_N(1:length(t_seq));
Phi_N          = cumsum(S_N)*Ts;  
psi        = exp(1i*2*pi*modulation_index*Phi_N);
if(state==0)
    if(alpha==-1)
        trans_state=psi.*ca_tilt(1:9);
    else
        trans_state=psi.*ca_tilt(1:9);
    end
else
    if(alpha==-1)
        trans_state=psi.*ca_tilt(17:25);
    else
        trans_state=psi.*ca_tilt(17:25);
    end
end

end

