function  [De_BER, Un_BER, De_FER, Un_FER, N_iter, dblk, lliatio_out, c_flag, BE_now, Un_BER_now, De_BER_now, Un_FER_now, De_FER_now] = decode_mat(hl_dec, llratio, InfoData, MB, NB, Z, De_BER, Un_BER, De_FER, Un_FER, N_iter, ped_bit)


KB = NB-MB;
InfoLength = KB * Z - ped_bit;% 消息长度

EncodeLength = length(llratio);
[lliatio_out, n, chk_dis] = step(hl_dec, llratio');
lliatio_out = lliatio_out';
dblk = zeros(1, EncodeLength);
for ii=1:1:EncodeLength
    dblk(ii) = 0;
    if (lliatio_out(ii)<0)
        dblk(ii) = 1;
    end
end
c_flag = sum(chk_dis);
% n = hl_dec.actualNumIterations;

N_iter = N_iter + n;% 更新迭代次数
Un_BER_now = 0;%  该数据块译码中产生的错误
De_BER_now = 0;
Un_FER_now = 0;%  该数据块译码中产生的错误
De_FER_now = 0;

for k =1: InfoLength
   if  InfoData(k) ~= dblk(ped_bit+k) 
	   if c_flag ~= 0 
           De_BER_now = De_BER_now + 1;% 错误可检测的错误比特数
       else
           Un_BER_now = Un_BER_now + 1;
       end
   end
end

if De_BER_now > 0 || Un_BER_now > 0
    error_flag=1;% 译码结果有错
else
    error_flag = 0;%  译码结果没有错
end

if error_flag == 1
	if c_flag ~= 0 
        De_FER_now = 1;% 可检测的错误块数		
    else
        Un_FER_now = 1;	
    end
end

BE_now = Un_BER_now + De_BER_now;
if (n==1)
    lliatio_out = llratio;
end

Un_BER = Un_BER + Un_BER_now;% 更新译码产生的错误
De_BER = De_BER + De_BER_now;
Un_FER = Un_FER + Un_FER_now;
De_FER = De_FER + De_FER_now;
    
end