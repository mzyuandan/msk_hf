function [hl_dec, hl_enc]= Get_SH_mod(MB,NB,Z, file_name)


spa_r = [];
spa_c = [];

%    读取原始的矩阵,并对QC_H进行不同Z_FAC下的模运算
QC_H = dlmread(file_name);
cnt = 0;
for ii = 1 : MB
    for jj = 1 : NB
        if QC_H(ii,jj)>=0
            tp = mod(QC_H(ii,jj), Z);
            QC_H(ii,jj) = tp;
            for kk=1:1:Z
                cnt = cnt + 1;
                spa_r(cnt) = (ii-1)*Z + kk; 
                spa_c(cnt) = (jj-1)*Z + mod(tp+kk-1, Z) + 1;
            end
        end
    end
end

spa = sparse(spa_r, spa_c, 1);
hl_enc = fec.ldpcenc(spa);
hl_dec = fec.ldpcdec(spa);

hl_dec.decisionType = 'Soft decision';
hl_dec.outputFormat = 'Whole codeword';
hl_dec.doParityChecks = 'Yes';

