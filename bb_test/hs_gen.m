clc;
clear all;
close all;

NB = 18;
KB = 6;
MB = 12;
Z =	60;
Z_MAX = 256;

file_name = 'hmx_1d3_12X18_128.txt';
save_name = 'sh_1d3_12X18_60_msk.mat';

[hl_dec, hl_enc]= Get_SH(MB,NB,Z,Z_MAX, file_name);
% [hl_dec, hl_enc]= Get_SH_mod(MB,NB,Z, file_name);
% [hl_dec, hl_enc]= Get_SH_w2(MB,NB,Z,Z_MAX, file_name);


save(save_name, 'hl_dec', 'hl_enc');
aa = 0;
