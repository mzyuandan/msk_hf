function [ g_t,q_t,t ] = pulse_gen( pulse, pulse_length, w, Fs, sps )
%PULSE_GEN 此处显示有关此函数的摘要
%   此处显示详细说明

g_t              = 0;
q_t              = 0;
time_sa          = 1/Fs;
switch             pulse
    
    case    1
        t          = -pulse_length/2:(1/sps):pulse_length/2;
        t0         = 0;
        g_t        = (2*w)./((t-t0).^2+w^2);
        g_t        = g_t*(1/(4*pi));
        Cst        = sum(g_t)*time_sa;
        nug_t      = (0.5) / Cst;
        g_t        = nug_t*g_t;
        q_t        = cumtrapz(g_t)*time_sa;
        
        
    case    2
        t           = -pulse_length/2:(1/sps):pulse_length/2;
        BT          = 0.3;
        alpha       = 2*pi*BT/(sqrt(log(2)));
        gauss       = qfunc(alpha*(t-0.5)) - qfunc(alpha*(t+0.5));
        Cst         = 0.5/(sum(gauss)*time_sa);
        g_t         = Cst*gauss;
        q_t         = cumtrapz(g_t)*time_sa;
        
    case    3
        t          = 0:(1/sps):pulse_length;
        g_t        = (1/(2*pulse_length).*(1- cos(2*pi.*t/(pulse_length))));
        K          = 0.5/(sum(g_t)*time_sa);
        g_t        = K*g_t;
        q_t        = cumtrapz(g_t)*time_sa;
        
        
    case   4
        t                      = 0:(1/sps):pulse_length;
        g_t                    = 1/(2*pulse_length)*ones(1,length(t));
        g_t(1)                 = 0;
%         g_t(end)               = 0;
        K                      = 0.5/(sum(g_t)*time_sa);
        g_t                    = K*g_t;
        q_t                    = cumtrapz(g_t)*time_sa;
        
    otherwise
        disp('no pulses for pulse >4')
end



end

