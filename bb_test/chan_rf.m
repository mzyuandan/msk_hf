function [ data_rec, data_rec_e, h ] = chan_rf( data_trans, ch, fd, snr,flag, M, mp, sps, sig_pow, BW_bb)
len = length(data_trans);

Ts = ch.InputSamplePeriod;
Fs = 1 / Ts;
path = round(ch.PathDelays ./ Ts);
pn = length(path);


if flag==0
    if fd==0
        data_rec_e = data_trans;
        pg = zeros(1, pn);
        for ii=2:1:pn
            pg(ii) = exp(1i*2*pi*rand(1,1));
            data_rec_e = data_rec_e + [zeros(1, path(ii)), data_trans(1:len-path(ii)) * pg(ii)];
        end
        data_rec_e = data_rec_e * (1/sqrt(pn));
        h = zeros(len,M*sps);
        for ii=1:1:pn
            h(:,path(ii)+mp) = 1/sqrt(pn) * pg(ii);
        end
    else
        data_rec_e = filter(ch, data_trans);
        path_gain = ch.PathGains;
    %     根据带宽进行修改
        h = zeros(len,M*sps);
        for ii=1:1:pn
            h(:,path(ii)+mp) = path_gain(:,ii);
        end
    end
else
    data_rec_e = data_trans;
    h = zeros(len,M*sps);
    h(:,mp)=1;
end
pw_fac = BW_bb /  (Fs/2);
snr_real = 10^(snr/10);
N = (sig_pow / snr_real) / pw_fac;
noise = sqrt(N/2) * (randn(1, length(data_rec_e))+1i*randn(1, length(data_rec_e)));
data_rec = data_rec_e + noise;

% data_rec = awgn(data_rec_e, snr, 10*log10(sig_pow/pw_fac));
end