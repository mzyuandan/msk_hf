function [ mod_signal, phn ] = msk_mod( syb_tx, g_t, Ts, os, modulation_index )
%MSK_MODE 此处显示有关此函数的摘要
%   此处显示详细说明

Nbits  = length(syb_tx); 
bits_s        = upsample(syb_tx,os);
t_seq         = 0:Ts:Nbits*Ts*os-Ts;
S_N           = conv(bits_s,g_t);
S_N           = S_N(1:length(t_seq));
phn         = cumsum(S_N)*Ts;

mod_signal    = exp(1i*2*pi*modulation_index*phn);
    

end

