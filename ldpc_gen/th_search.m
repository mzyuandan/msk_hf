clc;
clear all;
close all;


QC_H_ori = dlmread('hb_18X27M10.txt');
[mb, nb] = size(QC_H_ori);
kb = nb - mb;
evl_num = 50;
Qval_t = 10;
CodeRate = kb / nb;

%矩阵中0替换为-1，不需要则屏蔽掉此段程序
for ii=1:1:mb
    for jj=1:1:nb
        if (QC_H_ori(ii,jj)==0)
            QC_H_ori(ii,jj)=-1;
        else
            QC_H_ori(ii,jj) = QC_H_ori(ii,jj);
        end
            fprintf('%4d',QC_H_ori(ii,jj));
     end
    fprintf('\n');
end
fprintf('\n');

QC_H = QC_H_ori;
                        
col_w = zeros(1, nb);
for jj=1:1:nb
	for ii=1:1:mb
        if (QC_H(ii,jj)~=-1)
            col_w(jj) = col_w(jj) +1;
        end
    end
end
dv = max(col_w);
						
row_w = zeros(1, mb);
for ii=1:1:mb
	for jj=1:1:nb
        if (QC_H(ii,jj)~=-1)
            row_w(ii) = row_w(ii) +1;
        end
	end
end
dc = max(row_w);
						
var_vect = zeros(1, dv-1);
for jj=2:1:dv
    for xx=1:1:nb
        if (col_w(xx)==jj)
            var_vect(jj-1) = var_vect(jj-1) + 1; 
        end
    end
end

chk_vect = zeros(1, dc-1);
for ii=2:1:dc
    for xx=1:1:mb
        if (row_w(xx)==ii)
            chk_vect(ii-1) = chk_vect(ii-1) + 1; 
        end
    end
end

edg_num = 0;
for jj=2:1:dv
    edg_num = edg_num + jj*var_vect(jj-1);
end

lam = zeros(1, dv-1);
for jj=2:1:dv
    lam(jj-1) = jj*var_vect(jj-1) / edg_num;
end

rou = zeros(1, dc-1);
for ii=2:1:dc
    rou(ii-1) = ii*chk_vect(ii-1) / edg_num;
end


threshold = threshold_cal_ir_new1( lam, rou, evl_num, Qval_t, 0.0001, 1.1900 );	
SNR_t = 10*log10(1/(2*(threshold^2)));

a=0;



