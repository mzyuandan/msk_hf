%%%%基于16*32基矩阵生成Z=360,180,90,45都不含4,6环的矩阵
clc;
clear all;
close all;

hmx_1d4 = [ 293, 442,  -1,  -1,  37,   0,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1;
             -1,  -1,  -1,  64,  -1,   0,   0,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1;
             -1,  -1,  -1,   0,  -1,  -1,   0,   0,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1;
            346, 133,  -1,  -1,  -1,  -1,  -1,   0,   0,  -1,  -1,  -1,  -1,  -1,  -1,  -1;
             -1, 218, 384,  -1,  -1,  -1,  -1,  -1,   0,   0,  -1,  -1,  -1,  -1,  -1,  -1;
             -1,  -1,  -1, 421,   0,  -1,  -1,  -1,  -1,   0,   0,  -1,  -1,  -1,  -1,  -1;
             -1,  74,  96,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,   0,  -1,  -1,  -1,  -1;
             -1, 250,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,   0,  -1,  -1,  -1;
             -1,  -1,  -1, 272,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,   0,  -1,  -1;
             -1,  -1, 373, 384,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,   0,  -1;
            208, 261,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,   0;
             -1,  -1,  -1, 138,  37,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0;];
Z_Max_1d4 = 512;

          
%%
H_c = hmx_1d4;
z_max = 128;
z_max_x = Z_Max_1d4;
Z1 = 48;
Z2 = 128;
Z3 = 128;
Z4 = 128;
Z5 = 128;
Z6 = 128;
Z7 = 128;
Z8 = 128;
g4 = 0;
g6 = 0;
g4_pos = zeros(1000, 8);
g6_pos = zeros(3000,12);
[M, N] = size(H_c);
K = N - M;
% H_c = zeros(M,N);                      %%%输出的Zmax=256的矩阵
H_c_b0 = H_c; 
H_c_b1 = -1.*ones(M,N);                  %%Z1
H_c_b2 = -1.*ones(M,N);                  %%Z2
H_c_b3 = -1.*ones(M,N);                  %%Z3
H_c_b4 = -1.*ones(M,N);                  %%Z4
H_c_b5 = -1.*ones(M,N);                  %%Z5
H_c_b6 = -1.*ones(M,N);                  %%Z6
H_c_b7 = -1.*ones(M,N);                  %%Z5
H_c_b8 = -1.*ones(M,N);                  %%Z6
yy = zeros(N,1);                         %%存储变量节点的度
zz = zeros(M,1);                         %%存储校验节点的度

g4s = 1000*ones(1,8);
g6s = 1000*ones(1,8);
set_g4 = [0 1000 1000 1000 1000 1000 1000 1000];
set_g6 = [10 100000 100000 100000 100000 100000 100000 100000];
girth_tt = 7;
sum_l = 3;

for ii=2:1:M-1
    if (H_c(ii, K+1)>=0)
        xpos = ii;
        break;
    end
end

for ii=1:1:M
    for jj=1:1:N
        if (H_c(ii,jj)>=0)
            zz(ii)=zz(ii)+1;              %%计算校验节点的度
        end
    end
end
for ii=1:1:N
    for jj=1:1:M
        if (H_c(jj,ii)>=0)
            yy(ii)=yy(ii)+1;              %%计算校验节点的度
        end
    end
end

for ii=1:1:M
    for jj=1:1:N
        if (H_c_b0(ii,jj)>=0)
            H_c_b0(ii,jj) = floor((H_c_b0(ii,jj)*z_max)/z_max_x);
            H_c_b1(ii,jj) = floor((H_c_b0(ii,jj)*Z1)/z_max);
            H_c_b2(ii,jj) = floor((H_c_b0(ii,jj)*Z2)/z_max);
            H_c_b3(ii,jj) = floor((H_c_b0(ii,jj)*Z3)/z_max);
            H_c_b4(ii,jj) = floor((H_c_b0(ii,jj)*Z4)/z_max);
            H_c_b5(ii,jj) = floor((H_c_b0(ii,jj)*Z5)/z_max);
            H_c_b6(ii,jj) = floor((H_c_b0(ii,jj)*Z6)/z_max);
            H_c_b7(ii,jj) = floor((H_c_b0(ii,jj)*Z7)/z_max);
            H_c_b8(ii,jj) = floor((H_c_b0(ii,jj)*Z8)/z_max);
        else
            H_c_b1(ii,jj) = H_c(ii,jj);
            H_c_b2(ii,jj) = H_c(ii,jj);
            H_c_b3(ii,jj) = H_c(ii,jj);
            H_c_b4(ii,jj) = H_c(ii,jj);
            H_c_b5(ii,jj) = H_c(ii,jj);
            H_c_b6(ii,jj) = H_c(ii,jj);
            H_c_b7(ii,jj) = H_c(ii,jj);
            H_c_b8(ii,jj) = H_c(ii,jj);
        end
    end
end

for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b1(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b2(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b3(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b4(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b5(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b6(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b7(ii,jj));
    end
    fprintf('\n');
end
fprintf('\n');
for ii=1:1:M
    for jj=1:1:N
        fprintf('%4d', H_c_b8(ii,jj));
    end
    fprintf('\n');
end

[g4,g6,g4_pos,g6_pos,g4s(1),g6s(1)] = my_girth_4_6(H_c_b1,Z1,z_max,g4,g6,g4_pos,g6_pos, 1);
[g4,g6,g4_pos,g6_pos,g4s(2),g6s(2)] = my_girth_4_6(H_c_b2,Z2,z_max,g4,g6,g4_pos,g6_pos, 0);
[g4,g6,g4_pos,g6_pos,g4s(3),g6s(3)] = my_girth_4_6(H_c_b3,Z3,z_max,g4,g6,g4_pos,g6_pos, 0);
[g4,g6,g4_pos,g6_pos,g4s(4),g6s(4)] = my_girth_4_6(H_c_b4,Z4,z_max,g4,g6,g4_pos,g6_pos, 0);
[g4,g6,g4_pos,g6_pos,g4s(5),g6s(5)] = my_girth_4_6(H_c_b5,Z5,z_max,g4,g6,g4_pos,g6_pos, 0);
[g4,g6,g4_pos,g6_pos,g4s(6),g6s(6)] = my_girth_4_6(H_c_b6,Z6,z_max,g4,g6,g4_pos,g6_pos, 0);
[g4,g6,g4_pos,g6_pos,g4s(7),g6s(7)] = my_girth_4_6(H_c_b7,Z7,z_max,g4,g6,g4_pos,g6_pos, 0);
[g4,g6,g4_pos,g6_pos,g4s(8),g6s(8)] = my_girth_4_6(H_c_b8,Z8,z_max,g4,g6,g4_pos,g6_pos, 0);

 
%% 消4,6环
g4_min = sum(g4s(1:sum_l));
g6_min = sum(g6s(1:sum_l));
cnt_g6 = 0;
cnt_g4 = 0;
g6_flg = 0;

 while (1)
    if (girth_cmp(g4s, set_g4)==1 && girth_cmp(g6s, set_g6)==1)                        %%%无4,6环，输出结果
        for ii=1:1:M
            for jj=1:1:N
               fprintf('%4d',H_c_b0(ii,jj));
            end
            fprintf('\n');
        end
        fprintf('\n');
        break;
    end
    
    
    
    %%消6环
    while(1)
        if (girth_cmp(g4s, set_g4)==0)
            break;                         %%有4环，先消4环
        end
        if (girth_cmp(g6s, set_g6)==1)                         %%无6环，跳出
            break;
        end
        if (girth_cmp(g6s, set_g6)==0)  
            g6_flg = 1;
            
        
                            
            for ii=1:1:g6
               gogo = (mod((H_c_b1(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b1(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b1(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b1(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b1(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b1(g6_pos(ii,11),g6_pos(ii,12))),Z1)==0) || ...
                          (mod((H_c_b2(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b2(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b2(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b2(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b2(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b2(g6_pos(ii,11),g6_pos(ii,12))),Z2)==0) || ...
                          (mod((H_c_b3(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b3(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b3(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b3(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b3(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b3(g6_pos(ii,11),g6_pos(ii,12))),Z3)==0) || ...
                          (mod((H_c_b4(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b4(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b4(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b4(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b4(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b4(g6_pos(ii,11),g6_pos(ii,12))),Z4)==0)  || ...
                          (mod((H_c_b5(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b5(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b5(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b5(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b5(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b5(g6_pos(ii,11),g6_pos(ii,12))),Z5)==0)  || ...
                          (mod((H_c_b6(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b6(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b6(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b6(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b6(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b6(g6_pos(ii,11),g6_pos(ii,12))),Z6)==0)  || ...
                          (mod((H_c_b7(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b7(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b7(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b7(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b7(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b7(g6_pos(ii,11),g6_pos(ii,12))),Z7)==0)  || ...
                          (mod((H_c_b8(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b8(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b8(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b8(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b8(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b8(g6_pos(ii,11),g6_pos(ii,12))),Z8)==0);
               while(gogo)
                      if (zz(g6_pos(ii,1))>zz(g6_pos(ii,5)))
                          if (zz(g6_pos(ii,5))>zz(g6_pos(ii,9)))                %%改变校验节点度小的Z
                              ll = 9;
                              oo = 5;
                          else
                              ll = 5;
                              oo = 9;
                          end
                      else
                          if (zz(g6_pos(ii,1))>zz(g6_pos(ii,9)))
                              ll = 9;
                              oo = 1;
                          else
                              ll = 1;
                              oo = 9;
                          end
                      end
                      while (g6_pos(ii,ll+1)>K)
                           ll = ll + 2;
                           if (ll>12)
                               ll = 1;
                           end
                      end
                      

                      H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1)) = fix(z_max*rand);
                      H_c_b1(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z1/z_max);
                      H_c_b2(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z2/z_max);
                      H_c_b3(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z3/z_max);
                      H_c_b4(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z4/z_max);
                      H_c_b5(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z5/z_max);
                      H_c_b6(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z6/z_max);
                      H_c_b7(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z7/z_max);
                      H_c_b8(g6_pos(ii,ll),g6_pos(ii,ll+1)) = floor(H_c_b0(g6_pos(ii,ll),g6_pos(ii,ll+1))*Z8/z_max);
%                       if (H_c_b(1,17)~=gg)
%                            
%                       end
        
                      if ((mod((H_c_b1(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b1(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b1(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b1(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b1(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b1(g6_pos(ii,11),g6_pos(ii,12))),Z1)==0) || ...
                          (mod((H_c_b2(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b2(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b2(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b2(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b2(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b2(g6_pos(ii,11),g6_pos(ii,12))),Z2)==0) || ...
                          (mod((H_c_b3(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b3(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b3(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b3(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b3(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b3(g6_pos(ii,11),g6_pos(ii,12))),Z3)==0) || ...
                          (mod((H_c_b4(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b4(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b4(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b4(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b4(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b4(g6_pos(ii,11),g6_pos(ii,12))),Z4)==0) || ...
                          (mod((H_c_b5(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b5(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b5(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b5(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b5(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b5(g6_pos(ii,11),g6_pos(ii,12))),Z5)==0) || ...
                          (mod((H_c_b6(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b6(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b6(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b6(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b6(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b6(g6_pos(ii,11),g6_pos(ii,12))),Z6)==0) || ...
                          (mod((H_c_b7(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b7(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b7(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b7(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b7(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b7(g6_pos(ii,11),g6_pos(ii,12))),Z7)==0) || ...
                          (mod((H_c_b8(g6_pos(ii,1),g6_pos(ii,2)) - H_c_b8(g6_pos(ii,3),g6_pos(ii,4)) + H_c_b8(g6_pos(ii,5),g6_pos(ii,6)) -  ...
                               H_c_b8(g6_pos(ii,7),g6_pos(ii,8)) + H_c_b8(g6_pos(ii,9),g6_pos(ii,10)) - H_c_b8(g6_pos(ii,11),g6_pos(ii,12))),Z8)==0));
                          continue;
                      else
                          break;
                      end
               end
            end
         end
         [g4,g6,g4_pos,g6_pos,g4s(1),g6s(1)] = my_girth_4_6(H_c_b1,Z1,z_max,g4,g6,g4_pos,g6_pos, 1);
         [g4,g6,g4_pos,g6_pos,g4s(2),g6s(2)] = my_girth_4_6(H_c_b2,Z2,z_max,g4,g6,g4_pos,g6_pos, 0);
         [g4,g6,g4_pos,g6_pos,g4s(3),g6s(3)] = my_girth_4_6(H_c_b3,Z3,z_max,g4,g6,g4_pos,g6_pos, 0);
         [g4,g6,g4_pos,g6_pos,g4s(4),g6s(4)] = my_girth_4_6(H_c_b4,Z4,z_max,g4,g6,g4_pos,g6_pos, 0);
         [g4,g6,g4_pos,g6_pos,g4s(5),g6s(5)] = my_girth_4_6(H_c_b5,Z5,z_max,g4,g6,g4_pos,g6_pos, 0);
         [g4,g6,g4_pos,g6_pos,g4s(6),g6s(6)] = my_girth_4_6(H_c_b6,Z6,z_max,g4,g6,g4_pos,g6_pos, 0);
         [g4,g6,g4_pos,g6_pos,g4s(7),g6s(7)] = my_girth_4_6(H_c_b7,Z7,z_max,g4,g6,g4_pos,g6_pos, 0);
         [g4,g6,g4_pos,g6_pos,g4s(8),g6s(8)] = my_girth_4_6(H_c_b8,Z8,z_max,g4,g6,g4_pos,g6_pos, 0);
     
         if(girth_cmp(g4s, set_g4)==1 && sum(g6s(1:sum_l))<g6_min)         
          sum(g6s(1:sum_l)) 
          fprintf('\n');
          for ii=1:1:M
              for jj=1:1:N
                  fprintf('%4d', H_c_b0(ii,jj));
              end
              fprintf('\n');
          end
          fprintf('\n');
          g6_min = sum(g6s(1:sum_l));
          H_c_rec = H_c_b0;
         end
         cnt_g6 = cnt_g6 + 1;
    end
   
    %%消4环
    while(1)
        if (girth_cmp(g4s, set_g4)==1)                   %%无4环，跳出
            break;
        end
        if (girth_cmp(g4s, set_g4)==0)                      
%             for ii=1:1:M
%                 for jj=1:1:N
%                     if (H_c_b1(ii,jj)>=0)
%                         zz(ii)=zz(ii)+1;     %%计算校验节点的度     
%                     end
%                 end
%             end                     
            for ii=1:1:g4
               while(1)
                      if (zz(g4_pos(ii,1)) > zz(g4_pos(ii,5)))               %%%%改变校验节点度小的Z
                          ll = 5;
                      else
                          ll = 1;
                      end
                      if(g4_pos(ii,ll+1)>K)
                          ll=ll+2;
                      end
                          
                      H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1)) = fix(z_max*rand);
                      H_c_b1(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z1/z_max);
                      H_c_b2(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z2/z_max);
                      H_c_b3(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z3/z_max);
                      H_c_b4(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z4/z_max);
                      H_c_b5(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z5/z_max);
                      H_c_b6(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z6/z_max);
                      H_c_b7(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z7/z_max);
                      H_c_b8(g4_pos(ii,ll),g4_pos(ii,ll+1)) = floor(H_c_b0(g4_pos(ii,ll),g4_pos(ii,ll+1))*Z8/z_max);
                      
                      if ((mod((H_c_b1(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b1(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b1(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b1(g4_pos(ii,7),g4_pos(ii,8))),Z1)==0) || ...
                          (mod((H_c_b2(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b2(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b2(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b2(g4_pos(ii,7),g4_pos(ii,8))),Z2)==0) || ...
                          (mod((H_c_b3(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b3(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b3(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b3(g4_pos(ii,7),g4_pos(ii,8))),Z3)==0) || ...
                          (mod((H_c_b4(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b4(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b4(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b4(g4_pos(ii,7),g4_pos(ii,8))),Z4)==0) || ...
                          (mod((H_c_b5(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b5(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b5(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b5(g4_pos(ii,7),g4_pos(ii,8))),Z5)==0) || ...
                          (mod((H_c_b6(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b6(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b6(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b6(g4_pos(ii,7),g4_pos(ii,8))),Z6)==0) || ...
                          (mod((H_c_b7(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b7(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b7(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b7(g4_pos(ii,7),g4_pos(ii,8))),Z7)==0) || ...
                          (mod((H_c_b8(g4_pos(ii,1),g4_pos(ii,2)) - H_c_b8(g4_pos(ii,3),g4_pos(ii,4)) + H_c_b8(g4_pos(ii,5),g4_pos(ii,6)) - H_c_b8(g4_pos(ii,7),g4_pos(ii,8))),Z8)==0));
                          continue;
                      else
                          break;
                      end
               end
            end
        end
       [g4,g6,g4_pos,g6_pos,g4s(1),g6s(1)] = my_girth_4_6(H_c_b1,Z1,z_max,g4,g6,g4_pos,g6_pos, 1);
       [g4,g6,g4_pos,g6_pos,g4s(2),g6s(2)] = my_girth_4_6(H_c_b2,Z2,z_max,g4,g6,g4_pos,g6_pos, 0);
       [g4,g6,g4_pos,g6_pos,g4s(3),g6s(3)] = my_girth_4_6(H_c_b3,Z3,z_max,g4,g6,g4_pos,g6_pos, 0);
       [g4,g6,g4_pos,g6_pos,g4s(4),g6s(4)] = my_girth_4_6(H_c_b4,Z4,z_max,g4,g6,g4_pos,g6_pos, 0);
       [g4,g6,g4_pos,g6_pos,g4s(5),g6s(5)] = my_girth_4_6(H_c_b5,Z5,z_max,g4,g6,g4_pos,g6_pos, 0);
       [g4,g6,g4_pos,g6_pos,g4s(6),g6s(6)] = my_girth_4_6(H_c_b6,Z6,z_max,g4,g6,g4_pos,g6_pos, 0);
       [g4,g6,g4_pos,g6_pos,g4s(7),g6s(7)] = my_girth_4_6(H_c_b7,Z7,z_max,g4,g6,g4_pos,g6_pos, 0);
       [g4,g6,g4_pos,g6_pos,g4s(8),g6s(8)] = my_girth_4_6(H_c_b8,Z8,z_max,g4,g6,g4_pos,g6_pos, 0);
       if (g6_flg==0)
           cnt_g4 = cnt_g4 + 1;
       end
       if(girth_cmp(g4s, set_g4)==1 && sum(g6s(1:sum_l))<g6_min)         
          sum(g6s(1:sum_l))  
          fprintf('\n');
          for ii=1:1:M
              for jj=1:1:N
                  fprintf('%4d', H_c_b0(ii,jj));
              end
              fprintf('\n');
          end
          fprintf('\n');
          g6_min = sum(g6s(1:sum_l));
          H_c_rec = H_c_b0;
       end
      
    end
 end


    
        
        

            
            
 
          
        
       
    
    





